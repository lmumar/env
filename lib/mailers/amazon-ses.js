'use strict';

var NodeMailerMailer = require('./nodemailer-mailer');
var _ = require('lodash');

exports.create = function(options) {
  var config = options.config;
  var nodemailer = require('nodemailer');
  var ses = require('nodemailer-ses-transport');

  var transporter = nodemailer.createTransport(ses({
    accessKeyId: config.get('amazonses:accessKeyId'),
    secretAccessKey: config.get('amazonses:secretAccessKey')
  }));

  var sesMailer = new NodeMailerMailer(transporter, _.defaults({ }, options, {
    defaultFromEmailName: config.get('amazonses:defaultFromEmailName'),
    defaultFromEmailAddress: config.get('amazonses:defaultFromEmailAddress'),
    overrideTo: config.get('amazonses:overrideTo')
  }));

  return function(options) {
    return sesMailer.send(options);
  };
};
