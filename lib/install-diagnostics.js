'use strict';

var util = require('util');
var os = require('os');
var path = require('path');
var processName = require('./process-name')

function getDateIdentifier() {
  function pad(d) {
    if (d < 10) return "0" + d;
    return "" + d;
  }

  var d = new Date();
  return util.format('%s-%s-%s-%s%s%s',
    d.getUTCFullYear(),
    pad(d.getUTCMonth() + 1),
    pad(d.getUTCDate()),
    pad(d.getUTCHours()),
    pad(d.getUTCMinutes()),
    pad(d.getUTCSeconds()));
}

var installed = false;
exports.install = function(options) {
  if (installed) return;
  installed = true;

  /* Heapdump and nodemon don't play nicely together */
  if(process.env.NODEMON) return;

  /* Disable the default option for heapdump */
  process.env.NODE_HEAPDUMP_OPTIONS = 'nosignal';

  var heapdump = require('heapdump');

  var logger = options.logger;

  process.on('SIGUSR2', function() {
    var appName = processName.getFullProcessName();

    var filename = util.format('heap.%s.%s.%s.%s.heapsnapshot',
      os.hostname(),
      appName,
      process.pid,
      getDateIdentifier());

    filename = path.resolve('.', filename);

    logger.warn('Writing heapsnapshot: ' + filename);
    heapdump.writeSnapshot(filename, function(err, filename) {
      if (err) {
        logger.warn('Unable to write heapsnapshot: ', { exception: err });
        return;
      }

      logger.warn('Wrote heapsnapshot: ', { filename: filename });
    });
  });

};
