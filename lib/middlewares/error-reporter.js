/*jshint globalstrict: true, trailing: false, unused: true, node: true */
"use strict";

var debug = require('debug')('gitter:http-error');
var http = require('http');

/**
 * Returns an integer value is possible
 * or undefined otherwise
 */
function asStatusCode(value) {
  if (!value) return;

  if (typeof value === 'number') return value;

  if (typeof value === 'string') {
    var v = parseInt(value, 10);
    if (v && v >= 400 && v < 600) return v;
  }

  return;
}

/**
 * Given an error, return a status code and message and the message to send to
 * the client
 */
function getStatusAndMessage(err) {
  var errorIsStatusCode = asStatusCode(err);
  var message;

  // This deals with the deprecated use-case
  // of `throw 500` or `callback(500)`
  if(errorIsStatusCode) {
    message = http.STATUS_CODES[errorIsStatusCode] || http.STATUS_CODES[500];
    return {
      status: errorIsStatusCode,
      message: message,
      clientMessage: message
    };
  }

  var status = asStatusCode(err.status || err.statusCode) || 500;
  message = err.message || http.STATUS_CODES[status] || http.STATUS_CODES[500];
  var clientMessage = err.clientMessage || http.STATUS_CODES[status] || http.STATUS_CODES[500];

  return { status: status, message: message, clientMessage: clientMessage };
}

exports.create = function(options) {
  var logger = options.logger;
  var statsClient = options.statsClient;
  var errorReporter = options.errorReporter;

  function stat(name, req, additionalTags) {
    var user = req.user;
    var username = user && user.username;
    var tags = additionalTags || [];

    if (username) {
      tags.push('user:' + username);
  }

    if (req.path) {
      tags.push('path:' + req.path);
    }

    debug("stat: %s %j", name, tags);

    statsClient.increment(name, 1, 1, tags);
  }
  /* Has to have four args */
  return function(err, req, res, next) {
    var user = req.user;
    var userId = user && user.id;

    debug('Error: %s', err.stack);

    var statusAndMessage = getStatusAndMessage(err);
    var status = statusAndMessage.status;
    var message = statusAndMessage.message;
    var clientMessage = statusAndMessage.clientMessage;

    res.status(status);

    var errorIdentifer;
    switch(status) {
      case 401: stat('client_error_401', req); break;
      case 402: stat('client_error_402', req); break;
      case 403: stat('client_error_403', req); break;
      case 404: stat('client_error_404', req); break;
      case 429: stat('client_error_429', req); break;
      default:
        if(status >= 400 && status < 500) {
          stat('client_error_4xx', req, ['status:' + status]);
        } else if (status >= 500) {

          // Use a unique-ish identifier so that
          // users can report errors
          errorIdentifer = Date.now();

          // Send to sentry
          errorReporter(err, {
            type: 'response',
            status: status,
            userId: userId,
            username: user && user.username,
            url: req.url,
            method: req.method,
            route: req.routeIdentifier,
            clientMessage: clientMessage,
            errorIdentifer: errorIdentifer
          });

          // Send to statsd
          stat('client_error_5xx', req, ['status:' + status]);

          logger.error("An unexpected error occurred", {
             method: req.method,
             url: req.url,
             status: status,
             userId: userId,
             username: user && user.username,
             message: message,
             clientMessage: clientMessage,
             errorIdentifer: errorIdentifer
          });

          if(err.stack) {
            logger.error('Error: ' + err.stack);
          }
        }
    }

    res.locals.errorStatus = status;
    res.locals.errorMessage = clientMessage;
    res.locals.errorIdentifer = errorIdentifer;

    next(err);
  };
};
