'use strict';

module.exports = function identifyRouteMiddleware(routeName) {
  return function identifyRoute(req, res, next) {
    req.routeIdentifier = routeName;
    next();
  };
};
