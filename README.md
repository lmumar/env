gitter-env
==========

Deploying your node app? Use this to:

1. stop your app from crashing when redis fails over
2. keep servers alive by rotating logs
3. publish errors that wake people up in the night
4. publish stats that help make decisions
5. use different env configs so that you app can travel to new environments


## Using gitter-env

Firstly you need to create a singleton instance inside your application. Call the module something like env. 
It will need to reference the location of the configuration files, like this:

```
var gitterEnv = require('gitter-env');
var path = require('path');

/* Create a singleton environment */
module.exports = gitterEnv.create(path.join(__dirname, '..', '..', 'config'));

```

Once you've got your `env` instance, here are some things you can do with it

## Logging

`env.logger` looks a lot like [winston](https://github.com/flatiron/winston). Use it for logging:

* `logger.verbose(message, meta)`
* `logger.info(message, meta)`
* `logger.warn(message, meta)`
* `logger.error(message, meta)`

## Configuration

`env.conf` looks a lot like [nconf](https://github.com/flatiron/nconf). Use it for configuration:

* `conf.get('some:variable')`

## Error Reporter

`env.errorReporter` will send your errors to sentry. Use it for logging serious problems:

* `errorReporter(err, { some: 'meta', information: 1 })`

## Redis Client

`env.redis` will provide you with a singleton instance of a redis client or a new instance. It will use Redis Sentinel it it's configured.

Example configuration:
```json
{
  "redisDb": 1,
  "sentinel": {
    "master-name": "test-master",
    "hosts": ["localhost:46379"]
  },
  clientOpts: {
    "return_buffers": true
  }
}
```

* `env.redis.getClient()` - returns the main singleton Redis client.
* `env.redis.createClient([options])` - returns a new Redis client. Uses `redis` config by default.
* `env.redis.quitClient()` - closes a Redis client (don't do this with the main singleton client)

## Statistics

`env.stats` will send statistics to Datadog, Cube, Mixpanel and other places.

* `env.stats.event('name')` - increment a counter
* `env.stats.eventHF('name', [count], [frequency])` - increment a high frequency counter
* `env.stats.gaugeHF('name', [value], [frequency])` - update a high frequency gauge
* `env.stats.responseTime('name', duration)` - track a response time

## Error Handling

* `env.installUncaughtExceptionHandler` - will install a graceful shutdown handler on uncaught exceptions
* `env.domainWrap(function() { /* server */ });` - will do the same inside the function


## Middlewares

* `env.middlewares.errorHandler` - a basic error handling middleware
* `env.middlewares.accessLogger` - access logging with response time stats










