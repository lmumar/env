"use strict";

describe('gitter-stats', function() {
  it('should fire off all the events correctly', function() {
    var config = require('../lib/config').create(__dirname + '/config/stats');
    var logger = require('../lib/logger').create({ config: config });
    var stats = require('../lib/stats').create({ logger: logger, config: config });

    stats.event('testEvent');
    stats.eventHF('testEventHF');
    stats.gaugeHF('testGaugeHF');
    // stats.userUpdate('test');
    stats.responseTime('testResponseTime', 1);
  });

  it('should fire off google events', function(done) {
    var config = {
      get: function(name) {
        switch(name) {
          case 'stats:ga:enabled':
            return true;
          case 'stats:ga:key':
            return 'UA-45918290-3';
        }
      },
      getBool: function() {
        return false;
      },
      events: {
        on: function() {}
      }
    };
    var logger = require('../lib/logger').create({ config: config });
    var stats = require('../lib/stats').create({ logger: logger, config: config });

    stats.event('test', { googleAnalyticsUniqueId: '1644931976.1385378625' });
    setTimeout(done, 100);
  });


});
