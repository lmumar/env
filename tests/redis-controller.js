'use strict';

var assert = require('assert');
var spawn = require('child_process').spawn;
var fs = require('fs');
var temp = require('temp').track();
var debug = require('debug')('gitter:env:test:redis-controller');

function RedisController() {
  this.processes = {};
  this.dir = temp.mkdirSync('redistests');
  this.writeSentinelConfigs();
  this.options = {
    redisDb: 1,
    sentinel: {
      "master-name": "test-master",
      hosts: [
        "localhost:46379",
        "localhost:46380",
        "localhost:46381",
      ]
    }
  };
}

RedisController.prototype = {
  listenToProcess: function(name, process) {
    var processes = this.processes;
    debug('redis starting process %s', name)
    process.once('close', function() {
      debug('redis shutdown of process %s', name)
      delete processes[name];
    });
    return process;
  },

  startRedisPrimary: function() {
    var processes = this.processes;

    assert(!processes.redis1);
    processes.redis1 = this.listenToProcess('redis1', spawn('redis-server', [
      '--port', '36379',
      '--save', '',
      '--dir', this.dir
    ]));

    return processes.redis1;
  },

  startRedisSecondary: function() {
    var processes = this.processes;

    assert(!processes.redis2);
    processes.redis2 = this.listenToProcess('redis2', spawn('redis-server', [
      '--port', '36380',
      '--slaveof', 'localhost', '36379',
      '--save', '',
      '--dir', this.dir
    ]));

    return processes.redis2;
  },

  writeSentinelConfigs: function() {
    var dir = this.dir;

    fs.writeFileSync(dir + '/sentinel-1.conf', 'sentinel monitor test-master 127.0.0.1 36379 2\n' +
      'sentinel down-after-milliseconds test-master 500\n' +
      'sentinel failover-timeout test-master 1000\n' +
      'sentinel config-epoch test-master 5');

    fs.writeFileSync(dir + '/sentinel-2.conf', 'sentinel monitor test-master 127.0.0.1 36379 2\n' +
      'sentinel down-after-milliseconds test-master 500\n' +
      'sentinel failover-timeout test-master 1000\n' +
      'sentinel config-epoch test-master 5');

    fs.writeFileSync(dir + '/sentinel-3.conf', 'sentinel monitor test-master 127.0.0.1 36379 2\n' +
      'sentinel down-after-milliseconds test-master 500\n' +
      'sentinel failover-timeout test-master 1000\n' +
      'sentinel config-epoch test-master 5');
  },

  startSentinel: function(number) {
    var dir = this.dir;
    var processes = this.processes;

    assert(!processes['sentinel' + number]);
    var filename = dir + '/sentinel-' + number + '.conf';
    var port = 46378 + number;

    var p = processes['sentinel' + number] = this.listenToProcess('sentinel' + number, spawn('redis-server', [
      filename,
      '--sentinel',
      '--port', port,
      '--save', '',
      '--dir', dir]));

    return p;
  },

  stop: function(name, signal) {
    var processes = this.processes;

    if(!processes[name]) return;
    processes[name].kill(signal);
    delete processes[name];
  },

  ensureAllStarted: function() {
    var processes = this.processes;

    debug('ensuring the redis cluster is running');

    if (!processes.redis1) this.startRedisPrimary();
    if (!processes.redis2) this.startRedisSecondary();
    if (!processes.sentinel1) this.startSentinel(1);
    if (!processes.sentinel2) this.startSentinel(2);
    if (!processes.sentinel3) this.startSentinel(3);
  },

  stopAll: function() {
    Object.keys(this.processes).forEach(function(name) {
      this.stop(name);
    }, this);
  },

  cleanup: function() {
    this.stopAll();
    temp.cleanupSync();
  }
};

module.exports = RedisController;
