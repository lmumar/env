"use strict";

var assert = require('assert');
var redisClient = require('../lib/redis-client');
var mockConfig = require('./mock-config');
var RedisController = require('./redis-controller');
var redisSpec = require('./redis-spec');

describe('gitter-redis', function() {
  var logger;

  before(function() {
    logger = require('../lib/logger').create({ config: mockConfig({}) });
  });

  describe('node_redis tests', function() {
    var redisController;

    before(function(done) {
      redisController = new RedisController();
      done();
    });

    beforeEach(function() {
      redisController.ensureAllStarted();
    });

    after(function() {
      redisController.cleanup();
    });

    it('should obtain a connection and release it', function(done) {

      var mainRedisClient = redisClient.create(redisController.options, logger);
      assert.strictEqual(redisClient.testOnly.getClients().length, 1);
      assert(mainRedisClient);

      mainRedisClient.on('reconnected', function() {
        redisClient.quit(mainRedisClient);

        assert.strictEqual(redisClient.testOnly.getClients().length, 0);

        setImmediate(function() {
          // Don't cause problems for other tests after we shut down redis
          mainRedisClient.on('error', function() { });
          done();
        });
      });
    });

    it('should obtain a transient connection and release it', function(done) {
      var mainRedisClient = redisClient.create(redisController.options, logger);

      redisClient.createTransientClient(mainRedisClient, redisController.options, logger, function(err, temporaryClient) {
        assert.strictEqual(redisClient.testOnly.getClients().length, 2);

        assert(temporaryClient);

        redisClient.quit(temporaryClient);
        redisClient.quit(mainRedisClient);

        assert.strictEqual(redisClient.testOnly.getClients().length, 0);

        setImmediate(function() {
          // Don't cause problems for other tests after we shut down redis
          temporaryClient.on('error', function() { });
          mainRedisClient.on('error', function() { });
          done();
        });

      });

    });
  });

  redisSpec(function(options) {
    return redisClient.create(options, logger);
  }, function(client) {
    client.on('error', function() { });
    redisClient.quit(client);
  });


});
