"use strict";

var Promise = require('bluebird');
var assert = require('assert');
var mockConfig = require('./mock-config');
var stats = require('../lib/stats');
var logger = require('../lib/logger');
var mailer = require('../lib/mailer');

describe('mailer', function() {
  var mailerInstance, config, statsInstance, loggerInstance;

  beforeEach(function() {
    config = mockConfig({
      "mandrill": {
        "username": "admin@troupe.co",
        "apiKey": "p2GT6NcOhCVL_y_OadEdjQ"
      }
    });

    // TODO We could do with mocks of these
    statsInstance = stats.create({ config: config });
    loggerInstance = logger.create({ config: config });

    mailerInstance = mailer.create({
      config: config,
      stats: statsInstance,
      logger: loggerInstance
    });
  });

  it('should send emails', function(done) {
    mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: 'Guten tag',
          NAME: 'Mister Test'
        }
      })
      .then(function(result) {
        assert(result.status);
        assert(result.id);
      })
      .nodeify(done);
  });

  it('should send emails with bccs', function(done) {
    mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        bcc: 'test2@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: 'Guten tag',
          NAME: 'Mister Test'
        }
      })
      .then(function(result) {
        assert(result.status);
        assert(result.id);
      })
      .nodeify(done);
  });

  it('should send emails with unresolved promises', function(done) {
    mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: Promise.delay(10).thenReturn('Guten morgen'),
          NAME: Promise.delay(10).thenReturn('Mister promise')
        }
      })
      .then(function(result) {
        assert(result.status);
        assert(result.id);
      })
      .nodeify(done);
  });

});
